describe('Register endpoint', () => {
    beforeEach(() => {
        cy.fixture('registration/new_user.json').as('newUser');
        cy.fixture('registration/invalid_password_new_user.json').as('invalidPass');
    });

    it('should return 200 when valid request is made', () => {
        cy.get('@newUser').then((userJson) => {
            postUser(userJson).then((postResp) => {
                expect(postResp.status).to.eq(200);
            });
        });
    });

    it('should return 422 and an informative message when an invalid password is provided', () => {
        cy.get('@invalidPass').then((userJson) => {
            postUser(userJson).then((postResp) => {
                expect(postResp.status).to.eq(422);
                expect(postResp.body.errors.password).to.contain('The password must be at least 10 characters long and contain a mix of lower case and upper case letters, symbols and numbers.');
            });
        })
    });

    const postUser = (requestBody) => {
        return cy.apiRequest('POST', '/auth/register', requestBody);
    };
});