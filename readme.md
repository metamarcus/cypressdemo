# Cypress Demo for Lendflow

## Automation Exercises
### 2. Using Cypress or Javascript Selenium, write a script that will sign up for a Lendflow account
Submission:
```
cypress\integration\ui\01_ui_registration_spec.js
```

### 3. Given the google search "newest programming language", write a locator for the third suggestion of one of the Related Searches at the bottom of the page
Submission:
```
cypress\integration\ui\02_ui_google_search_spec.js
```

## Install dependencies
```
npm install --save-dev
```

## Running the tests
### Headless
```
npx cypress run
```

### With Visual Test Runner
```
npx cypress open
```