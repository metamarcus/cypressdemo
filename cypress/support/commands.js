// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
Cypress.Commands.add('apiRequest', (requestMethod, endpoint, requestBody = null) => {
    cy.getCookie('XSRF-TOKEN')
        .should('exist')
        .then((xsrfCookie) => {
            cy.request({
                method: 'GET',
                url: Cypress.env('api_server') + '/csrf-token',
                headers: {
                    'x-xsrf-token': xsrfCookie.value
                },
            });
            cy.request({
                method: requestMethod,
                url: Cypress.env('api_server') + endpoint,
                failOnStatusCode: false,
                body: requestBody,
                headers: {
                    'x-xsrf-token': xsrfCookie.value,
                },
            });
        });

});

Cypress.Commands.add('xsrfAuthRequest', (email, password) => {
    cy.request({
        method: 'GET',
        url: Cypress.env('api_server') + '/csrf-token',
    });
    cy.apiRequest('POST', '/authentication/log-in', { email: email, password: password });
});

// Captcha needs be configured for test mode
Cypress.Commands.add('completeGoogleCaptcha', () => {
    // ToDo: Find an implicit way to wait on the captcha to render
    cy.wait(500);
    cy.window().then(win => {
        win.document
            .querySelector("iframe[src*='recaptcha']")
            .contentDocument.getElementById("recaptcha-token")
            .click();
    });
});

//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
