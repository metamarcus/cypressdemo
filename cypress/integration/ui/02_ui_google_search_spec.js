describe('Google Search', () => {
    it('query in searchbox should change when third Related Search is clicked', () => {
        cy.visit('https://www.google.com');
        cy.get('[title=Search]').type('newest programming language{enter}');
        cy.get('[id=botstuff]').get('[class=EIaa9b]>div').find('div[data-hveid]*').eq(2).click();
        cy.get('[name=q]').should('not.have.text', 'newest programming language');
    });
});