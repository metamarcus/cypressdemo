describe('Register Page', () => {
    beforeEach(() => {
        cy.visit(Cypress.env('app_url') + '/register');
    });

    it('should have all fields necessary to register and have a disabled submit button', () => {
        cy.get('[name=first_name]').should('be.visible');
        cy.get('[name=last_name]').should('be.visible');
        cy.get('[name=company_name]').should('be.visible');
        cy.get('[name=company_website]').should('be.visible');
        cy.get('[name=email]').should('be.visible');
        cy.get('[name=password]').should('be.visible');
        cy.get('[name=password_confirmation]').should('be.visible');
        cy.get('[type=submit]').should('be.disabled');
    });

    it('should allow a user with valid information to register', () => {
        fillOutRegistrationForm('Test', 'User', 'Test Corp', 'https://www.test.org', generateRandomEmail(), '(713) 650-0426', 'MyT3stP@ssw0rd');
        cy.completeGoogleCaptcha();
        cy.get('[type=submit]').click();
        cy.contains('Thank you for registering with Lendflow');
    });

    it('should have a disabled submit button when captcha is not complete', () => {
        fillOutRegistrationForm('Bob', 'Odinkirk', 'Saul Goodman Industries', 'https://www.test.org', 'saul_goodman@sharklasers.com', '(713) 650-0426', 'MyT3stP@ssw0rd');
        cy.get('[type=submit]').should('be.disabled');
    });

    it('should not allow a user with an invalid password to register', () => {
        fillOutRegistrationForm('Test', 'User', 'Test Corp', 'https://www.test.org', generateRandomEmail(), '(713) 650-0426', 'a');
        cy.completeGoogleCaptcha();
        cy.get('[type=submit]').click();
        cy.contains('The password must be at least 10 characters long and contain a mix of lower case and upper case letters, symbols and numbers');
        cy.get('[type=submit]').should('be.disabled');
    });

    it('should not allow a user with an invalid phone number to register', () => {
        fillOutRegistrationForm('Test', 'User', 'Test Corp', 'https://www.test.org', generateRandomEmail(), '(123) 456-7890', 'MyT3stP@ssw0rd');
        cy.completeGoogleCaptcha();
        cy.get('[type=submit]').click();
        cy.contains('The phone field contains an invalid number');
        cy.get('[type=submit]').should('be.disabled');
    });

    const fillOutRegistrationForm = (firstName, lastName, companyName, companyWebsite, companyEmail, phone, pass) => {
        cy.get('[name=first_name]').type(firstName);
        cy.get('[name=last_name]').type(lastName);
        cy.get('[name=company_name]').type(companyName);
        cy.get('[name=company_website]').type(companyWebsite);
        cy.get('[name=email]').type(companyEmail);
        cy.get('[name=password]').type(phone);
        cy.get('[name=password_confirmation]').type(pass);
    };

    const generateRandomEmail = () => {
        return 'lendflow_test_user_' + Math.floor((Math.random() * 100000) + 1) + '@sharklasers.com';
    };
});